import { Provider } from 'react-redux';

import Root               from './components/root';
import LocalStorageHelper from './helpers/localStorage';
import 'font-awesome/css/font-awesome.css';
import './style.css';

LocalStorageHelper.init();

const render = () => {
  ReactDOM.render(
    <Provider store={LocalStorageHelper.getStore()}>
      <Root/>
    </Provider>,
    document.getElementById('app')
  );
};

window.onload = function () {
  render();
};

if (_DEV_) {
  window.onbeforeunload = () => LocalStorageHelper.saveState();
}