import { createStore } from 'redux';

export default class LocalStorageHelper {

  /**
   *
   * @type {null}
   * @private
   */
  static _initialState = null;

  /**
   *
   * @type {null}
   * @private
   */
  static _store        = null;

  static init() {
    const state = window.localStorage.getItem('todo:state');
    LocalStorageHelper._initialState = state ? JSON.parse(state) : {};

    const reducers = require('../reducers');
    const createStoreFn   = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() ?
      window.__REDUX_DEVTOOLS_EXTENSION__()(createStore) /* create store with devTools */ : createStore /* create store as it is */;

    LocalStorageHelper._store = createStoreFn(reducers);

    if (_DEV_) {
      if (module.hot) {
        module.hot.accept('../reducers', () => {
          const nextReducer = require('../reducers');
          LocalStorageHelper._store.replaceReducer(nextReducer);
        });
      }
    }
  }

  /**
   *
   * @returns {*}
   */
  static getStore() {
    return LocalStorageHelper._store;
  }

  /**
   *
   */
  static saveState() {
    window.localStorage.setItem('todo:state', JSON.stringify(LocalStorageHelper.getStore().getState()));
  }

  /**
   *
   * @returns {*}
   */
  static getState() {
    if (!LocalStorageHelper._initialState) {
      LocalStorageHelper.init();
    }

    return LocalStorageHelper._initialState;
  }
}