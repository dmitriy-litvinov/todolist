export default class ActionsEnum {

  static TODO = {
    ADD_TODO: 'ADD_TODO',
    ADD_FORM: 'ADD_FORM'
  };

  static TOGGLE_DONE = 'TOGGLE_DONE';
  static SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';
  static DELETE_TODO = 'DELETE_TODO';
};