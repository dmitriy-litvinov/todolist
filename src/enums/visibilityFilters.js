export default class VisibilityFiltersEnum {

  static ALL = 'ALL';
  static ACTIVE = 'ACTIVE';
  static DONE = 'DONE';

};