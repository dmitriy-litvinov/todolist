import { connect } from 'react-redux';
import { addTodo, addTodoForm } from '../../actions/todo';
import './style.css';

class InputComponent extends React.Component {

  constructor(props) {
    super(...arguments);

    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  static defaultProps = {
    addForm: {
      data: {text: ''},
      error: ''
    }
  };

  /**
   *
   * @param
   */
  onSubmit(evt) {
    evt.preventDefault();
    const target = evt.target || evt.srcElement;
    const el = target.todoTextInput;
    const elVal = el.value.trim();

    if (!elVal) {
      alert('Todo text is required!!!');
      return;
    }

    const todo = {
        list:[{text: el.value.trim(), done: false, index: null}],
        addForm: {
          data: {text: ''},
          error: ''
        }
      };

    this.props.dispatch(addTodo(todo));
    el.value = '';
  }

  onBlur(evt) {
    return false;
    const el = evt.target || evt.srcElement;

    el.value = '';

    const addForm = {
        data: {text: ''},
        error: ''
      };

    this.props.dispatch(addTodoForm(addForm));
  }

  onChange(evt) {
    const el = evt.target || evt.srcElement;
    const elVal = el.value.trim();

    const addForm = {
      data: {text: elVal},
      error: ''
    };

    this.props.dispatch(addTodoForm(addForm));
  }

  /**
   *
   * @returns {XML}
   */
  render() {

    return (
      <form className="add_todo" name="addTodoForm" onSubmit={this.onSubmit} autoComplete="off">
        <input type="text" defaultValue="" name="todoText" id="todoTextInput" onChange={this.onChange} onBlur={this.onBlur} placeholder="Enter text..."/>

        <input type="submit" disabled={this.props.addForm.data.text.trim().length ? '' : 'disabled'} value={'Add'}/>
      </form>
    );
  }

}

const mapStateToProps = state => {
  const {addForm} = state.todo;

  return {
    addForm: addForm
  };
};

export default connect(mapStateToProps)(InputComponent); // get dispatch in props