import './style.css';

import { connect } from 'react-redux';
import VisibilityFiltersEnum from '../../enums/visibilityFilters';
import { toggleDone, deleteTodo  } from '../../actions/todo';


class ListComponent extends React.Component {

  static defaultProps = {
    todo: []
  };

  constructor(props) {
    super(...arguments);

    this.newTodo = this.newTodo.bind(this);
    this.getList = this.getList.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
  }

  /**
   *
   * @param
   */
  newTodo(evt) {
    evt.preventDefault();
    let elemID = evt.target.getAttribute('data-id');
    this.props.dispatch(toggleDone(elemID));
  }

  removeTodo(evt) {
    let elemID = evt.target.getAttribute('data-id');
    this.props.dispatch(deleteTodo(elemID));
  }

  /**
   *
   * @returns {Array}
   */
  getList() {
    const liEls = [];

    this.props.todo.forEach((todo, i) => {
      liEls.push(
        <li key={i}>
          <span data-id={todo.index}
                onClick={this.newTodo}
                className={`todo_item ${todo.done ? ' done': ''}`}
          >
            {todo.text}
          </span>
          <i title={`remove ${todo.text}`}
             data-id={todo.index}
             aria-hidden="true"
             className="fa fa-times todo_remove"
             onClick={this.removeTodo}
          ></i>
        </li>)
    });

    return liEls;
  }


  /**
   *
   * @returns {XML}1
   */
  render() {
    return (
      <ul id="list">{ this.props.todo.length ? this.getList() : <li className="list_empty">No items</li> }</ul>
    );
  }

}

/**
 *
 * @param state
 * @returns {{todo: Array}}
 */
const mapStateToProps = state => {

  const { todo, visibilityFilter } = state;
  let filteredTodos = [];

  todo.list.forEach(function (todo, i) {
    return todo.index = i;
  });

  if (visibilityFilter === VisibilityFiltersEnum.ALL) {
    filteredTodos = todo.list;
  } else {
    filteredTodos = todo.list.filter(item => {
      return visibilityFilter === VisibilityFiltersEnum.ACTIVE ? !item.done : item.done;
    });
  }

  return {
    todo: filteredTodos
  };
};

export default connect(mapStateToProps)(ListComponent);