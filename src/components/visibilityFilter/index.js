import './style.css';
import { connect } from 'react-redux';
import { setVisibilityFilter } from '../../actions/visibilityFilter';
import VisibilityFiltersEnum from '../../enums/visibilityFilters';

class VisibilityFilterComponent extends React.Component {

  static defaultProps = {
    filter: VisibilityFiltersEnum.ALL
  };

  _filters = [
    { value: VisibilityFiltersEnum.ALL, text: 'All' },
    { value: VisibilityFiltersEnum.ACTIVE, text: 'Active' },
    { value: VisibilityFiltersEnum.DONE, text: 'Done' },
  ];

  constructor(props) {
    super(...arguments);

    this.onClick    = this.onClick.bind(this);
    this.getFilters = this.getFilters.bind(this);
  }

  /**
   *
   * @param evt
   */
  onClick(evt) {
    evt.preventDefault();

    this.props.dispatch(setVisibilityFilter(evt.target.id));
  }

  getFilters () {
    return this._filters.map((item, i) => {
      return (
        <a
          href="#"
          key={i}
          id={item.value}
          onClick={this.onClick}
          className={item.value === this.props.filter ? 'active' : ''}
        >
          {item.text}
        </a>
      );
    });
  }

  /**
   *
   * @returns {XML}
   */
  render() {

    return (
        <div id="visibilityFilter" className={this.props.hasTodos ? '' : ' none'}>{ this.getFilters() }</div>
    );
  }

}

const mapStateToProps = state => {

  return {
    filter:   state.visibilityFilter,
    hasTodos: !!state.todo.list.length
  };
};

export default connect(mapStateToProps)(VisibilityFilterComponent);