import { connect } from 'react-redux';

import Input from './input';
import VisibilityFilter from './visibilityFilter';
import List from './list';

class RootComponent extends React.Component {

  render() {

    return (
        <div id="root">
          <Input/>
          <VisibilityFilter/>
          <List/>
        </div>
    );

  };

}

export default connect()(RootComponent); // connect RootComponent to redux store changes in order watch it and get dispatch() in props