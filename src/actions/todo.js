import ActionsEnum from '../enums/actions';

const dafaultTodo = {
    list:[],
    addForm: {
      data: {text: ''},
      error: ''
    }
  };

export const addTodo = (todo = dafaultTodo) => {

  return {
    type: ActionsEnum.TODO.ADD_TODO,
    todo
  };
};

export const addTodoForm = (addForm = dafaultTodo.addForm) => {
  return {
    type: ActionsEnum.TODO.ADD_FORM,
    addForm
  };
};

export const toggleDone = (id = -1) => {
  return {
    type: ActionsEnum.TOGGLE_DONE,
    id
  };
};

export const deleteTodo = (id = -1) => {
  return {
    type: ActionsEnum.DELETE_TODO,
    id
  };
};