import ActionsEnum from '../enums/actions';
import VisibilityFiltersEnum from '../enums/visibilityFilters';

export const setVisibilityFilter = (filter = VisibilityFiltersEnum.ALL) => {
  return {
    type: ActionsEnum.SET_VISIBILITY_FILTER,
    filter
  }
};
