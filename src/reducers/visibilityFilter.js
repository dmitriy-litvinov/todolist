import ActionsEnum           from '../enums/actions';
import VisibilityFiltersEnum from '../enums/visibilityFilters';
import LocalStorageHelper    from '../helpers/localStorage';

const initialState = LocalStorageHelper.getState().visibilityFilter || VisibilityFiltersEnum.ALL;

export default (state = initialState, action) => {

  switch (action.type) {

    case ActionsEnum.SET_VISIBILITY_FILTER: {
      return action.filter;
    }

    default:
      return state;

  }

};
