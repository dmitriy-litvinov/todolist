import ActionsEnum        from '../enums/actions';
import LocalStorageHelper from '../helpers/localStorage';

const dafaultTodo = {
  list:[],
  addForm: {
    data: {text: ''},
    error: ''
  }
};

const initialState = LocalStorageHelper.getState().todo || dafaultTodo;

export default (state = initialState, action) => {

  switch (action.type) {

    case ActionsEnum.TODO.ADD_TODO: {
      let copy = Object.assign({} , state);
      copy.list = copy.list.concat(action.todo.list);
      copy.addForm = action.todo.addForm;
      return copy;
    }

    case ActionsEnum.TODO.ADD_FORM: {
      let copy = Object.assign({} , state);
      copy.addForm = action.addForm;
      return copy;
    }

    case ActionsEnum.TOGGLE_DONE: {
      let copy = Object.assign({} , state);
      let list = Object.assign([] , copy.list);
      const item =  list[action.id];

      item ? item.done = !item.done : null;
      copy.list = list;
      return copy;
    }

    case ActionsEnum.DELETE_TODO: {
      let copy = Object.assign({} , state);
      let list = Object.assign([] , copy.list);
      list.splice(action.id, 1);
      copy.list = list;
      return copy;
    }

    default:
      return state;

  }

};
