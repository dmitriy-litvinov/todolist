import { combineReducers } from 'redux';

import todo           from './todo';
import visibilityFilter from './visibilityFilter';

export default combineReducers(
  /*
    object = structure of store, where keys names are the properties of store and their values are corresponding reducers
   */
  {
    todo,
    visibilityFilter
  }
);